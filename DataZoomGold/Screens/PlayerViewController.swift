//
//  PlayerViewController.swift
//  DataZoomGold
//
//  Created by Milos Mokic on 21/07/2020.
//  Updated by Vlada Calic on July 2021
//  Copyright © 2020 iOS Developer. All rights reserved.
//

import Foundation
import AVKit
import DZ_Collector_iOS

class PlayerViewController: UIViewController {
  
  //MARK : - Public properties
  weak var collector: DZ_Collector? {
    didSet {
      collector?.adDelegate = self
    }
  }

  // MARK: - Properties
  private let contentURLString: String
  private let videoType: VideoType
  private let adType: AdType
  private let autoStartRecordingEvents: Bool
  
  // MARK: - Player
  private var playerViewController = AVPlayerViewController()
  private var contentPlayer: AVPlayer?
  private var playerLayer: AVPlayerLayer?
  private var observers: [String: NSKeyValueObservation] = [:]

  private lazy var observingSwitch: UISwitch! = {
    let observingSwitch = UISwitch()
    observingSwitch.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
    return observingSwitch
  }()
    
  // MARK: - Initializers
  init(contentURLString: String, videoType: VideoType, adType: AdType, autoStartRecordingEvents: Bool) {
    self.contentURLString = contentURLString
    self.videoType = videoType
    self.adType = adType
    self.autoStartRecordingEvents = autoStartRecordingEvents
    super.init(nibName: nil, bundle: nil)
  }
    
  @available(*, unavailable)
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
    
  // MARK: - View lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.black
    setUpContentPlayer()
    if autoStartRecordingEvents {
      observingSwitch.setOn(true, animated: false)
      switchValueDidChange(observingSwitch)
    }
  }
    
  deinit {
    collector?.stopRecordingEvents()
    playerViewController.player?.replaceCurrentItem(with: nil)
    playerViewController.player = nil
    NotificationCenter.default.removeObserver(self)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    playerViewController.player?.play()
    
    Global.perform(in: 0.1) { [weak self] in
      self?.requestInitialAd()
    }
  }
}

// MARK: - Private Helpers
extension PlayerViewController {
  
  private func requestInitialAd() {
    if videoType.shouldRequestAd, let url = adType.url {
      collector?.requestAds(for: url, in: view, viewController: self)
    } else {
    }
  }

  private func setUpContentPlayer() {
    guard let player = try? AVPlayer.from(urlString: contentURLString) else { Log.error("Cannot create player"); return }
    playerViewController = AVPlayerViewController()
    playerViewController.player = player
    setObservers(for: player.currentItem)
    collector?.setContentPlayhead(for: player)
    showContentPlayer()
    observers["videoBounds"] = playerViewController.observe(\.videoBounds, options: [.new]) { [weak self] in
      guard let collector = self?.collector,  let rect = $1.newValue else { return }
      collector.updateBounds(bounds: rect)
    }
    DispatchQueue.main.asyncAfter(deadline: .now() + 4) { [weak self] in
      guard let self = self else { return }
      self.addPushButton()
      self.addObservingSwitch()
    }
  }
}

// MARK: - Observing
extension PlayerViewController {
  func setObservers(for item: AVPlayerItem?) {
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(playerDidFinishPlaying),
      name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
      object: item
    )
    
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(contentDidFinishPlaying(_:)),
      name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
      object: item)
  }
  
  @objc func contentDidFinishPlaying(_ notification: Notification) {
    collector?.contentComplete()
  }
    
  @objc func playerDidFinishPlaying(note: NSNotification) {
    dismiss(animated: true)
  }
}

// MARK: - DataZoom
extension PlayerViewController {
  @objc func buttonPushTouched() {
    collector?.addCustom(event: "buttonPush", metadata: [:])
  }
    
  func addPushButton() {
    let frame = CGRect(x: (playerViewController.view.frame.width/2) - 45, y: 50, width: 90, height: 40)
    let button = UIButton(frame: frame)
    button.layer.cornerRadius = 10.0
    button.accessibilityLabel = "Push Me"
    button.isAccessibilityElement = true
    button.setTitle("Push Me", for: .normal)
    button.setTitleColor(UIColor.white, for: .normal)
    button.backgroundColor = UIColor(red: 34/255, green: 218/255, blue: 34/255, alpha: 1.0)
    button.addTarget(self, action: #selector(buttonPushTouched), for: .touchUpInside)
    view.addSubview(button)
  }
    
  func addObservingSwitch() {
    let label = UILabel()
    label.text = "Collect events: "
    label.textColor = .white
    let stackView = UIStackView(arrangedSubviews: [label, observingSwitch])
    let width = CGFloat(180)
    let frame = CGRect(x: (playerViewController.view.frame.width/2) - width/2, y: 100, width: width, height: 40)
    stackView.frame = frame
    stackView.axis = .horizontal
    view.addSubview(stackView)
  }
    
  @objc func switchValueDidChange(_ sender: UISwitch) {
    guard let player = playerViewController.player else { Log.error("GoldApp: NO PLAYER"); return }
    if sender.isOn {
      collector?.startRecordingEvents(for: player)
    } else {
      collector?.stopRecordingEvents()
    }
  }
}

// MARK: - AdController Delegate
extension PlayerViewController: DZ_AdDelegate {
  
  func play() {
    showContentPlayer()
    playerViewController.player?.play()
  }
  
  func pause() {
    playerViewController.player?.pause()
    hideContentPlayer()
  }

  private func showContentPlayer() {
    addChild(playerViewController)
    playerViewController.view.frame = view.bounds
    view.insertSubview(playerViewController.view, at: 0)
    playerViewController.didMove(toParent: self)
  }
  
  private func hideContentPlayer() {
    // The whole controller needs to be detached so that it doesn't capture events from the remote.
    playerViewController.willMove(toParent: nil)
    playerViewController.view.removeFromSuperview()
    playerViewController.removeFromParent()
  }
}
