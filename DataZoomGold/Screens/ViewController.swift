//
//  ViewController.swift
//  DataZoomGold
//
//  Created by iOS Developer on 06/03/18.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import UIKit
import DZ_Collector_iOS

var kAppDelegate = UIApplication.shared.delegate as! AppDelegate

var kId = ""

class ViewController: UIViewController, UITextViewDelegate {
  // MARK: - Outlets

  @IBOutlet private var videoTypeTextField: UITextField!
  @IBOutlet private var adTypeTextField: UITextField!
  @IBOutlet private var playVideoButton: UIButton!
  @IBOutlet private var fieldConfigId: UITextView!
  @IBOutlet private var connectingURLLabel: UILabel!
  @IBOutlet private var environmentSegment: UISegmentedControl!
  @IBOutlet private var chromecastHolder: UIView!

  @IBOutlet private var customUrlView: UIView!
  @IBOutlet private var customUrlTextField: UITextField!
  @IBOutlet private var adTagUrlView: UIView!
  @IBOutlet private var adTagUrlTextField: UITextField!
    
  @IBOutlet private var startDatazoomSDK: UISwitch!
  @IBOutlet private var autoStartRecordingSwitch: UISwitch!
  @IBOutlet private var autoStartRecordingContainer: UIView!
  @IBOutlet weak var customEventView: UIView!
  @IBOutlet weak var customEventLabel: UILabel!
  @IBOutlet weak var customEventTextField: UITextField!
  @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
  // MARK: - Properties
  private var sdk: SDK = SDK()
  private var collector: DZ_Collector?
  private var videoTypePickerView = UIPickerView()
  private var adTypePickerView = UIPickerView()
  private var doneButtonVideoType: UIBarButtonItem!
  private var doneButtonAdType: UIBarButtonItem!
    
  private var configId = ""

  private var connectionURL = ""
  private var buttonPushMe: UIButton?
    
  private let videoTypes = VideoType.allCases
  private let adTypes = AdType.allCases
  private var selectedVideoType: VideoType = .mp4 {
    didSet {
      videoTypeTextField.text = selectedVideoType.title
            
      UIView.animate(withDuration: 0.5) {
        self.customUrlView.isHidden = !self.selectedVideoType.shouldEnterCustomUrl
        self.view.layoutIfNeeded()
      }

      doneButtonVideoType.title = selectedVideoType.shouldEnterCustomUrl ? .next : .done
    }
  }
  private var warningShown = false
  private var sentCustomEvents: Set<String> = []
  @IBOutlet weak var stackView: UIStackView!
  
  private var selectedAdType: AdType = .none {
    didSet {
      adTypeTextField.text = selectedAdType.title
            
      UIView.animate(withDuration: 0.3) {
        self.adTagUrlView.isHidden = !self.selectedAdType.shouldEnterCustomUrl
        self.view.layoutIfNeeded()
      }
            
      doneButtonAdType.title = selectedAdType.shouldEnterCustomUrl ? .next : .done
    }
  }

  private var selectedEnvironment: Environment! {
    didSet {
      connectingURLLabel.text = selectedEnvironment.urlString
      connectionURL = selectedEnvironment.urlString
    }
  }
    
  // MARK: - View lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    autoStartRecordingContainer.isHidden = true
    startDatazoomSDK.setOn(false, animated: false)
    configId = fieldConfigId.text
        
    setupPickers()
    setupUI()
    setupDebug()
  }
    
  @IBAction private func sdkSwitchValueChanged(_ sender: UISwitch) {
    if sender.isOn {
      sdkOn()
    } else {
      sdkOff()
    }
  }
    
    
  private func showSimpleAlert(with message: String) {
    let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    present(alert, animated: true)
  }
    
  func showInvalidConfigIdAlert() {
    let alert = UIAlertController(title: "", message: .invalidConfigId, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    present(alert, animated: true)
  }
    
  @IBAction private func didChangeEnvironment(_ sender: Any) {
    let value = environmentSegment.selectedSegmentIndex
    selectedEnvironment = Environment(rawValue: value)
  }
    
  func sendCustomEvent() {
    guard let collector = collector else { return }
    guard let customEventText = customEventTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
    if customEventText == "" || sentCustomEvents.contains(customEventText) { return }

    collector.addCustom(event: customEventText, metadata: [:])
    sentCustomEvents.insert(customEventText)
  }
    
  @IBAction private func playVideo() {
    showPlayer()
  }
  
  func showPlayer() {
    let vc = PlayerViewController(
      contentURLString: selectedVideoType.url,
      videoType: selectedVideoType,
      adType: selectedAdType,
      autoStartRecordingEvents: autoStartRecordingSwitch.isOn
    )
    vc.collector = collector
    vc.modalPresentationStyle = .overFullScreen
    present(vc, animated: true, completion: nil)
    sendCustomEvent()
  }
}

// MARK: - Debug
extension ViewController {
  private func setupDebug() {
    #if DEBUG
    selectedVideoType = .mp4
    selectedAdType = .none
    fieldConfigId.text = kId
    configId = fieldConfigId.text
    environmentSegment.selectedSegmentIndex = Environment.staging.rawValue
    selectedEnvironment = .staging
    #endif
  }
}

// MARK: - Setup UI methods

extension ViewController {
  private func setupPickers() {
    let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    doneButtonVideoType = UIBarButtonItem(title: .done, style: .plain, target: self, action: #selector(pickerDoneVideoType))
    let cancelButtonVideoType = UIBarButtonItem(title: .cancel, style: .plain, target: self, action: #selector(pickerCancelVideoType))
    doneButtonAdType = UIBarButtonItem(title: .done, style: .plain, target: self, action: #selector(pickerDoneAdType))
    let cancelButtonAdType = UIBarButtonItem(title: .cancel, style: .plain, target: self, action: #selector(pickerCancelAdType))
    
    videoTypePickerView.delegate = self
    videoTypePickerView.dataSource = self
    videoTypePickerView.backgroundColor = .white
    videoTypeTextField.inputView = videoTypePickerView
    videoTypeTextField.inputAccessoryView = createToolbar(with: [cancelButtonVideoType, spaceButton, doneButtonVideoType])
    
    adTypePickerView.delegate = self
    adTypePickerView.dataSource = self
    adTypePickerView.backgroundColor = .white
    adTypeTextField.inputView = adTypePickerView
    adTypeTextField.inputAccessoryView = createToolbar(with: [cancelButtonAdType, spaceButton, doneButtonAdType])
  }
  
  private func setupUI() {
    customUrlTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    adTagUrlTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    customEventTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
//        customUrlTextField.delegate = self
    adTagUrlTextField.delegate = self
    customEventTextField.delegate = self
    
    customUrlTextField.returnKeyType = .next
    adTagUrlTextField.returnKeyType = .done
    
    environmentSegment.selectedSegmentIndex = Environment.defaultEnvironment.rawValue
    selectedEnvironment = .defaultEnvironment
    
    customUrlView.isHidden = true
    adTagUrlView.isHidden = true
    customEventView.isHidden = true
    
    selectedVideoType = .defaultType
    selectedAdType = .defaultType
    
  }

  private func createToolbar(with items: [UIBarButtonItem]) -> UIToolbar {
    let toolBar = UIToolbar()
    toolBar.barStyle = UIBarStyle.default
    toolBar.isTranslucent = false
    toolBar.tintColor = .black
    toolBar.sizeToFit()
    toolBar.setItems(items, animated: false)
    toolBar.isUserInteractionEnabled = true
    return toolBar
  }
}

// MARK: - UIPickerViewDelegate & UIPickerViewDataSource

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
    
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if pickerView == videoTypePickerView {
      return videoTypes.count
    } else if pickerView == adTypePickerView {
      return adTypes.count
    }
    return 0
  }
    
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    if pickerView == videoTypePickerView {
      return videoTypes[row].title
    } else if pickerView == adTypePickerView {
      return adTypes[row].title
    }
    return nil
  }
    
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    if pickerView == videoTypePickerView {
      selectedVideoType = videoTypes[row]
    } else if pickerView == adTypePickerView {
      selectedAdType = adTypes[row]
    }
//        pickerView.resignFirstResponder()
  }
}

// MARK: - UITextFieldDelegate

extension ViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField == adTagUrlTextField {
      textField.resignFirstResponder()
    }
    return true
  }

  func textViewDidChange(_ textView: UITextView) {
    configId = fieldConfigId.text
  }
  
  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    if text == "\n" {
      textView.resignFirstResponder()
      return false
    }
    return true
  }
}

// MARK: - UI Event handlers

extension ViewController {
  @objc func textFieldDidChange(_ textField: UITextField) {
    let text = textField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
      
    if textField == customUrlTextField, selectedVideoType.shouldEnterCustomUrl {
      selectedVideoType = .customUrl(text)
    } else if textField == adTagUrlTextField, selectedAdType.shouldEnterCustomUrl {
      selectedAdType = .customUrl(text)
    }
  }
  
  @objc private func pickerDoneVideoType() {
    let row = videoTypePickerView.selectedRow(inComponent: 0)
    selectedVideoType = videoTypes[row]
      
    if selectedVideoType.shouldEnterCustomUrl {
      customUrlTextField.becomeFirstResponder()
    } else {
      videoTypeTextField.resignFirstResponder()
    }
  }
  
  @objc private func pickerCancelVideoType() {
    videoTypeTextField.resignFirstResponder()
  }
  
  @objc private func pickerDoneAdType() {
    let row = adTypePickerView.selectedRow(inComponent: 0)
    selectedAdType = adTypes[row]
      
    if selectedAdType.shouldEnterCustomUrl {
      adTagUrlTextField.becomeFirstResponder()
    } else {
      adTypeTextField.resignFirstResponder()
    }
  }
  
  @objc private func pickerCancelAdType() {
    adTypeTextField.resignFirstResponder()
  }
}


extension ViewController {
  private func sdkOn() {
    if !Tracking.shared.canTrack && !warningShown {
      // showSimpleAlert(with: "Tracking not enabled, advertisingId will be ignored.")
      showToast(string: "Tracking not enabled, advertisingId will be ignored.")
    }
    activityIndicator.startAnimating()
    initializeSDK { [self] success in
      DispatchQueue.main.async {
      warningShown = true
       activityIndicator.stopAnimating()
        if success {
          autoStartRecordingContainer.isHidden = false
          customEventView.isHidden = false
        } else {
          startDatazoomSDK.setOn(false, animated: true)
          autoStartRecordingContainer.isHidden = true
          customEventView.isHidden = true
        }
      }
    }
  }
  
  private func sdkOff() {
    autoStartRecordingContainer.isHidden = true
    customEventView.isHidden = true
    collector?.deinitDatazoomSDK()
  }


  private func initializeSDK(completion: @escaping (Bool) -> ()) {
    if !validateParams() {
      completion(false)
      return
    }
    
    sdk.start(configId: configId, url: connectionURL) { [self] (collector, error) in
      if let c = collector {
        self.collector = c
        let metadata: [String: String] = [
          "timestamp": Date().debugDescription,
          "randomUUID": UUID().uuidString
        ]
        c.addCustom(metadata: metadata)
        completion(true)
      } else {
        if error != nil {
          showInvalidConfigIdAlert()
        }
        completion(false)
      }
    }
  }
}

private enum ValidationError: Error {
  case invalidConfigId, invalidUrl, invalidAdTag
  
  var value: String {
    switch self {
      case .invalidConfigId:
        return String.invalidConfigId
      case .invalidUrl:
        return String.enterValidUrl
      case .invalidAdTag:
        return String.enterValidAdTag
    }
  }
}

private extension ViewController {

  private func _validateParams() throws  {
    guard sdk.validateConfigId(configId: configId) else {
      throw ValidationError.invalidConfigId
    }
        
    if selectedVideoType.shouldEnterCustomUrl, sdk.validateCustomUrl(selectedVideoType.url) == false {
      throw ValidationError.invalidUrl
    }
        
    if selectedAdType.shouldEnterCustomUrl, sdk.validateAdTagUrl(selectedAdType.url!) == false {
      throw ValidationError.invalidAdTag
    }
  }
  
  private func validateParams() -> Bool {
    do {
      try _validateParams()
      return true
    } catch (let error) {
      guard let error = error as? ValidationError else {
        showSimpleAlert(with: "Error")
        return false
      }
      showSimpleAlert(with: error.value)
      return false
    }
  }
}
