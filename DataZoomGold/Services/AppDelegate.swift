//
//  AppDelegate.swift
//  DataZoomGold
//
//  Created by iOS Developer on 06/03/18.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import UIKit
// import DZ_Collector_iOS

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      Tracking.shared.askConsent()
      SDK.reset()
      return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func setWindow() {
        window = UIWindow.init(frame: UIScreen.main.bounds)
       // window?.makeKeyAndVisible()
    }
}






