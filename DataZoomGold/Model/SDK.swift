//
//  SDK.swift
//  DataZoomGold
//
//  Created by Vlada Calic on 12/04/2021.
//

import Foundation
import DZ_Collector_iOS

class SDK {
  private var adapter: NativeAdapter!
  private var collector: DZ_Collector!
  
  func start(configId: String, url: String, completion: @escaping ((DZ_Collector?, Error?) -> Void)) {
    collector = DZ_Collector(configId: configId, url: url)
    collector.start { [self] (isSuccess, error) in
      if let e = error {
        Log.debug(e)
        Log.debug(type(of: e))
        Log.debug("GoldApp: Error \(e.localizedDescription)")
        completion(nil, e)
      }
      completion(collector, nil)
      sendSDKLoadedEvent()
    }
  }
  
  private func sendSDKLoadedEvent() {
    // let eventName = "Datazoom_Loaded"
    let eventName = "GoldSDKLoaded"
    let metadata = ["Hello": "Datazoom"]
    collector.addCustom(event: eventName, metadata: metadata)
  }
}


extension SDK {
  func validateCustomUrl(_ urlString: String) -> Bool {
      return URL(string: urlString) != nil
  }
  
  func validateAdTagUrl(_ adTagUrl: String) -> Bool {
      return URL(string: adTagUrl) != nil
  }
  
  func validateConfigId(configId: String) -> Bool {
      if configId.range(of: "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", options: .regularExpression, range: nil, locale: nil) != nil {
          return true
      } else {
          return false
      }
  }
  
  static func reset() {
    DZ_Collector.appStarted()
  }
}
