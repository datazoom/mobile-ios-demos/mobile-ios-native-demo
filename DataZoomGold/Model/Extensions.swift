//
//  Extensions.swift
//  DataZoomGold
//
//  Created by Vlada Calic on 12/04/2021.
//  Copyright © 2021 iOS Developer. All rights reserved.
//

import Foundation
import AVFoundation

extension String {
  static let verifiedConfigId = "Configuration Id Verified."
  static let invalidConfigId = "Please enter a valid Configuration Id."
  static let enterValidUrl = "Please enter a valid video url."
  static let enterValidAdTag = "Please enter a valid ad tag url."
  static let done = "Done"
  static let cancel = "Cancel"
  static let next = "Next"
  static let developmentURL = "https://devplatform.datazoom.io/beacon/v1/"
  static let stagingURL = "https://stagingplatform.datazoom.io/beacon/v1/"
  static let preProductionURL = "https://demoplatform.datazoom.io/beacon/v1/"
  static let productionURL = "https://platform.datazoom.io/beacon/v1/"
  static let notSupported = "Not supported yet."
  
  static func format(double: Double) -> String {
    let interval = Int(double)
    let seconds = interval % 60
    let minutes = (interval / 60) % 60
    let hours = (interval / 3600)
    return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
  }
}

extension AVPlayer {
  
  static var noPlayerError: Error {
    return NSError(domain: "AVPlayer", code: 404, userInfo: [:])
  }
  
  var title: String? {
    guard let asset = currentItem?.asset as? AVURLAsset else { return nil }
    return asset.url.lastPathComponent
  }
  
  static func from(urlString: String) throws -> AVPlayer {
    guard let contentURL = URL(string: urlString) else { throw AVPlayer.noPlayerError }
    let urlAsset = AVURLAsset(url: contentURL)
    let playerItem = AVPlayerItem(asset: urlAsset)
    return AVPlayer(playerItem: playerItem)
  }
}


class Global {
  /// Peforms block on a main thread in given number of seconds.
  /// - Parameters:
  ///   - in: Number of seconds in which to perform block.
  ///   - block: Block to perform.
  static func perform(in seconds: Float, block: @escaping () -> Void) {
    let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(seconds)/1000)
    DispatchQueue.main.asyncAfter(deadline: deadline, execute: block)
  }
  
  /// Peforms block on a background thread in given number of seconds.
  /// - Parameters:
  ///   - in: Number of seconds in which to perform block.
  ///   - block: Block to perform.
  static func performInBackground(in seconds: Float, block: @escaping () -> Void) {
    let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(seconds)/1000)
    DispatchQueue.global(qos: .background).asyncAfter(deadline: deadline, execute: block)
  }
}

extension TimeInterval {
  var stringValue: String {
    if self < 0 {
      return NSLocalizedString("LIVE", comment: "LIVE")
    }
    let formatter = DateComponentsFormatter()
    formatter.zeroFormattingBehavior = .pad
    formatter.allowedUnits = [.minute, .second]
    
    if self >= 3600 {
      formatter.allowedUnits.insert(.hour)
    }
    
    return formatter.string(from: self)!
  }
}
