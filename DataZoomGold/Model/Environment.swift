//
//  Environment.swift
//  DataZoomGold
//
//  Created by Milos Mokic on 31/07/2020.
//  Copyright © 2020 iOS Developer. All rights reserved.
//

import Foundation

enum Environment: Int {
    case development = 0
    case staging = 1
    case preProduction = 2
    case production = 3
    
    var urlString: String {
        switch self {
        case .development:
            return .developmentURL
        case .staging:
            return .stagingURL
        case .preProduction:
            return .preProductionURL
        case .production:
            return .productionURL
        }
    }
    
    static var defaultEnvironment: Self {
        return .staging
    }
}
